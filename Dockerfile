
FROM bauman/alpine:3.13-jupyter-full
ARG PACKING_MANIFEST
ARG NO_AGENTS
ARG NO_RECONFIG

USER root

RUN apk update && \
    pip3 install --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain && \
    rm -rf /var/cache/apk/*

RUN sed -i 's/3gp/pdf/'g  /usr/lib/python3.8/site-packages/notebook/static/tree/js/main.min.js
COPY run.sh  /usr/bin/run-notebook
RUN  chmod +x /usr/bin/run-notebook

USER jovyan
COPY notebook .jupyter/jupyter_notebook_config.json


CMD [ "run-notebook" ]