# Documentation Manifest

- [x] SET PACKING_MANIFEST environment variable to restrict which plugin documentation should be packaged
- [x] SET NO_AGENTS env if you don't want the agents packaged
- [x] SET NO_RECONFIG env if you don't want the reconfig tools packaged
- [x] Run a pipeline for the latest brach to overwrite the current latest branch
- [x] Run the integration stack witht he same PACKING_MANIFEST (WAIT UNTIL COMPLETE)
- [x] unset the PACKING_MANIFEST from this repo
- [x] re-run the latest brach pipeline on this repo


# Other help


Open a web browser and navigate to http://0.0.0.0:8888 to access the notebook.